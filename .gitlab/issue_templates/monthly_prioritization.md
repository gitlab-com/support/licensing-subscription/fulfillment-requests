<!--
ISSUE TEMPLATE: https://gitlab.com/gitlab-com/support/licensing-subscription/fulfillment-requests/-/blob/main/.gitlab/issue_templates/monthly_prioritization.md

Issue Title: YYYY Month - Monthly Prioritization of Fulfillment Issues and Epics
-->

<!-- SET A DUE DATE -->

## Introduction

On a monthly basis, L&R Support reviews each of the issues and epics that have the ~"Support Priority::Categorize" or ~"Support Priority::Review" label in order to determine how to prioritize them. The first step in the process is to create a Priority Score for each item. The second step is for the L&R Support [Regional DRIs](https://about.gitlab.com/handbook/support/license-and-renewals/#regional-dris) to determine the Support Priority (1 - 4) based on the scores. The final step is to review with the Fulfillment PMs the impact of these changes on our Top 10 and Next 10 lists.

## Process

Through this issue, we will track the creation of the priority scores. We ask that the L&R Support Engineers volunteer to review one or more issues or epics by taking the following steps:

1. Look at the ~"Support Priority::Categorize" and ~"Support Priority::Review" lists in each of these issue boards:
   1. GitLab.org: [Fulfillment Support Priority (issues)](https://gitlab.com/groups/gitlab-org/-/boards/2543339?label_name[]=section%3A%3Afulfillment)
   1. GitLab.org: [Support Priority (epics)](https://gitlab.com/groups/gitlab-org/-/epic_boards/39981?label_name[]=Support%20Priority&label_name[]=devops%3A%3Afulfillment)
   1. fulfillment-meta: [Fulfillment Support Priority (issues)](https://gitlab.com/gitlab-org/fulfillment/meta/-/boards/5530967?label_name[]=Support%20Priority)
1. Select an item from one of the lists and use the [L&R Support - Priority Calculator spreadsheet](https://docs.google.com/spreadsheets/d/1pjntNPQ_7F8ZTWvGxl8w4msMDg2tYTIMQqGP-5O2WUY/edit?usp=sharing) to create a priority score
   - You might want to make a temporary copy of the spreadsheet so that you don't get stepped on by anybody else trying to calculate at the same time
1. Edit the Description field of the issue or epic:
   1. Insert a blank line at the end, and then a horizontal rule `---`
   1. **Copy cell F18 from the spreadsheet and paste it on the next line of the Description**
   1. Save your work
1. Add the ~"Support Priority::Scored" label 
1. Create a new comment in THIS issue with:
   - Issue or epic link
   - Issue or epic title
   - The priority score, **as copied from the priority score calculator**
   - Any brief comments that you feel are important to explaining the score you gave

cc: @fulfillment-group/product-managers

<!-- ONCE CREATED, NOTIFY TEAM IN #support_licensing-subscription -->

/assign @gitlab-com/support/licensing-subscription
/label ~"Support Priority::Monthly"
