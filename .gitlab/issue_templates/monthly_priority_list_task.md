# Final List: New Priority 1 and 2 Lists; New Items in Priorities 3 and 4

<details>
<summary>

## New Support Priority 1 (AKA Top 10 List)

</summary>

| Issue/Epic | Title | Parent Epic | Score | Previous Position |
|------------|-------|-------------|-------|-------------------|
| link here | title here | null | score here | previous position here |

</details>

<details>
<summary>

## New Support Priority 2 (AKA Next 10 List)

</summary>

| Issue/Epic | Title | Parent Epic | Score | Previous Position |
|------------|-------|-------------|-------|-------------------|
| link here | title here | null | score here | previous position here |

</details>

<details>
<summary>

## New Support Priority 3 (Only showing items new to this priority)

</summary>

| Issue/Epic | Title | Parent Epic | Score | Previous Position |
|------------|-------|-------------|-------|-------------------|
| link here | title here | null | score here | previous position here |

</details>

<details>
<summary>

## New Support Priority 4 (Only showing items new to this priority)

</summary>

| Issue/Epic | Title | Parent Epic | Score | Previous Position |
|------------|-------|-------------|-------|-------------------|
| link here | title here | null | score here | previous position here |

</details>
